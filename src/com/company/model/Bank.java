package com.company.model;

/**
 * Created by Student on 05.12.2017.
 */
public class Bank extends Exchenger implements Credit {
    public float convert(int uah) {
        if (uah < 150_000) {
            return (uah / USD) * .95F;
        } else {
            return 0;
        }
    }

    @Override
    public int getCredit() {
        return 0;
    }
}
