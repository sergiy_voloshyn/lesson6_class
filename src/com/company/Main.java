package com.company;

import com.company.model.*;
import com.company.model.Exchenger;

import java.util.Objects;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Object[] organizations=new Object[3];
        organizations[0]=new Bank();
        organizations[1]=new BlackExchenger();
        organizations[2]=new WesternUnion();
        for (Object organization: organizations){
            if (organization instanceof Exchenger){
                ((Exchenger) organization).convert(5000);

            }

            if (organization instanceof Sender){
                ((Sender) organization).sendMoney(3000);
            }

            if (organization instanceof  Credit){
                ((Credit) organization).getCredit();
            }

        }



    }
}
